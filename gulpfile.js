const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');

gulp.task('serve', ['sass', 'concat-js'], () => {
  gulp.watch('./ressources/assets/sass/**/*.scss', ['sass']);
  gulp.watch('./ressources/assets/js/**/*.js', ['contact-js']);
});

gulp.task('concat-js', () => {
    return gulp.src(['./ressources/assets/js/jquery.js', './ressources/assets/js/bootstrap.js', './ressources/assets/js/app.js'])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/js'));
});

gulp.task('sass', () => {
  return gulp.src('./ressources/assets/sass/*.scss')
    .pipe(sass())
    .pipe(cleanCSS())
    .pipe(gulp.dest('./public/css'));
});

gulp.task('default', ['serve']);
