<?php

ini_set('display_errors', E_ALL);

session_start();

use Illuminate\Database\Capsule\Manager as Capsule;
use Slim\Router;
use Slim\Views\Twig as View;
use Slim\Csrf\Guard as Csrf;
use Intelis\App;
use Intelis\Middlewares\CsrfViewMiddleware;
use Intelis\Middlewares\ValidationErrorsMiddleware;
use Intelis\Middlewares\OldInputMiddleware;

require __DIR__ . '/../vendor/autoload.php';

$app = new App;

$dotenv = new \Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

$container = $app->getContainer();

$capsule = new Capsule();
$capsule->addConnection([
    'driver' => getenv('DB_DRIVER'),
    'host' => getenv('DB_HOST'),
    'port' => getenv('DB_PORT'),
    'database' => getenv('DB_DATABASE'),
    'username' => getenv('DB_USERNAME'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

require __DIR__ . '/../config/routes.php';

// $app->add($container->get(Csrf::class));
// $app->add(new CsrfViewMiddleware($container->get(Router::class), $container->get(View::class)));
// $app->add(new ValidationErrorsMiddleware($container->get(Router::class), $container->get(View::class)));
$app->add(new OldInputMiddleware($container->get(Router::class), $container->get(View::class)));
