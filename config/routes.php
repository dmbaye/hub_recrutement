<?php

use Slim\Router;
use Slim\Views\Twig as View;
use Intelis\Middlewares\AuthMiddleware;
use Intelis\Middlewares\AdminMiddleware;
use Intelis\Middlewares\GuestMiddleware;
use Intelis\Middlewares\RedirectIfNotAdminMiddleware;
use Intelis\Middlewares\RedirectIfAdminMiddleware;
use Intelis\Middlewares\AdminAuthMiddleware;

$app->get('/', ['Intelis\Controllers\HomeController', 'index'])->setName('home');
$app->get('/about', ['Intelis\Controllers\HomeController', 'about'])->setName('about');

// Auth routes
$app->group('', function () {
    $this->get('/register', ['Intelis\Controllers\AuthController', 'showRegister'])->setName('auth.register');
    $this->get('/login', ['Intelis\Controllers\AuthController', 'showLogin'])->setName('auth.login');
    $this->post('/register', ['Intelis\Controllers\AuthController', 'register']);
    $this->post('/login', ['Intelis\Controllers\AuthController', 'login']);
    $this->get('/admin/login', ['Intelis\Controllers\Admin\AdminController', 'showLogin'])->setName('admin.login');
    $this->post('/admin/login', ['Intelis\Controllers\Admin\AdminController', 'login']);
})->add(new GuestMiddleware($container->get(Router::class), $container->get(View::class)));

// Admin routes
$app->group('', function () {
    $this->get('/admin', ['Intelis\Controllers\Admin\AdminController', 'index'])->setName('admin');
    $this->get('/admin/experts', ['Intelis\Controllers\Admin\ProsController', 'index'])->setName('admin.experts');
})->add(
    new AdminAuthMiddleware($container->get(Router::class), $container->get(View::class)),
    new AdminMiddleware($container->get(Router::class), $container->get(View::class))
    // new RedirectIfNotAdminMiddleware($container->get(Router::class), $container->get(View::class))
);

// Dashboard routes
$app->group('', function () {
    $this->get('/logout', ['Intelis\Controllers\AuthController', 'logout'])->setName('auth.logout');
    $this->get('/{username}', ['Intelis\Controllers\DashboardController', 'index'])->setName('dashboard');
    $this->get('/{username}/profile', ['Intelis\Controllers\AuthController', 'profile'])->setName('auth.profile');
    $this->get('/{username}/edit', ['Intelis\Controllers\UserController', 'edit'])->setName('user.edit');
    $this->post('/{username}/update', ['Intelis\Controllers\UserController', 'updateInfo'])->setName('user.update.info');

    // Studies
    $this->get('/{username}/studies', ['Intelis\Controllers\StudiesController', 'index'])->setName('dashboard.studies');
    $this->get('/{username}/studies/new', ['Intelis\Controllers\StudiesController', 'new'])->setName('dashboard.studies.new');
    $this->post('/{username}/studies/new', ['Intelis\Controllers\StudiesController', 'create']);

    // Experiences
    $this->get('/{username}/experiences', ['Intelis\Controllers\ExperiencesController', 'index'])->setName('dashboard.experiences');
    $this->get('/{username}/experiences/new', ['Intelis\Controllers\ExperiencesController', 'new'])->setName('dashboard.experiences.new');
    $this->post('/{username}/experiences/new', ['Intelis\Controllers\ExperiencesController', 'create']);
})->add(
    new AuthMiddleware($container->get(Router::class), $container->get(View::class)),
    new RedirectIfAdminMiddleware($container->get(Router::class), $container->get(View::class))
);
