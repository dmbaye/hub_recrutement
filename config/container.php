<?php

use Psr\Container\ContainerInterface;
use function DI\get;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Slim\Flash\Messages as Flash;
use Slim\Csrf\Guard as Csrf;
use Intelis\Auth\Auth;
use Awurth\SlimValidation\ValidatorExtension;
use Awurth\SlimValidation\Validator;
// use Intelis\Validation\Validator;

return [
    Twig::class => function (ContainerInterface $c) {
        $view = new Twig(__DIR__ . '/../ressources/views', [
            'cache' => false,
        ]);

        $view->addExtension(new TwigExtension($c->get('router'), $c->get('request')->getUri()));
        $view->addExtension(new ValidatorExtension($c->get(Validator::class)));

        $view->getEnvironment()->addGlobal('auth', $c->get(Auth::class));
        $view->getEnvironment()->addGlobal('flash', $c->get(Flash::class));

        return $view;
    },
    Flash::class => function (ContainerInterface $c) {
        return new Flash;
    },
    Csrf::class => function (ContainerInterface $c) {
        return new Csrf;
    },
    Auth::class => function (ContainerInterface $c) {
        return new Auth;
    },
    Validator::class => function (ContainerInterface $c) {
        return new Validator($showValidationRules = true);
    }
];
