<?php

namespace Intelis\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
use Intelis\Models\Experience;
use Intelis\Models\User;

class ExperiencesController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $experiences = Experience::where('user_id', $_SESSION['user'])
            ->orderBy('finished_at', 'desc')
            ->get();

        return $this->view->render($response, 'dashboard/experiences/index.twig', [
            'experiences' => $experiences,
        ]);
    }

    public function new(Request $request, Response $response)
    {
        return $this->view->render($response, 'dashboard/experiences/new.twig');
    }

    public function create(Request $request, Response $response)
    {
        // Validate user inputs
        $validation = $this->validator->validate($request, [
            'company' => v::notEmpty()->stringType(),
            'position' => v::notEmpty()->stringType(),
            'tasks' => v::notEmpty()->stringType(),
        ]);

        $user = User::findOrFail($_SESSION['user']);

        if ($validation->failed()) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et reéssayez');
            return $response->withRedirect($this->router->pathFor('dashboard.experiences.new', ['username' => $user->username]));
        }

        $started = "{$request->getParam('start_year')}/{$request->getParam('start_month')}/{$request->getParam('start_day')}";
        $finished = "{$request->getParam('graduation_year')}/{$request->getParam('graduation_month')}/{$request->getParam('graduation_day')}";

        $experience = new Experience();

        $experience->user_id = $_SESSION['user'];
        $experience->company = $request->getParam('company');
        $experience->position = $request->getParam('position');
        $experience->tasks = $request->getParam('tasks');
        $experience->started_at = $started;
        $experience->finished_at = $finished;

        if (!$experience->save()) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et re2ssayez');
            return $response->withRedirect($this->router->pathFor('dashboard.experiences.new', ['username' => $user->username]));
        }

        $this->flash->addMessage('success', 'Votre expérience à été ajouter');
        return $response->withRedirect($this->router->pathFor('dashboard', ['username' => $user->username]));
    }
}
