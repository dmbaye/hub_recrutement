<?php

namespace Intelis\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as v;
use Intelis\Models\User;
use Intelis\Models\Study;

class StudiesController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $studies = Study::where('user_id', $_SESSION['user'])
            ->orderBy('finished_at', 'desc')
            ->get();

        return $this->view->render($response, 'dashboard/studies/index.twig', [
            'studies' => $studies,
        ]);
    }

    public function new(Request $request, Response $response)
    {
        return $this->view->render($response, 'dashboard/studies/new.twig');
    }

    public function create(Request $request, Response $response)
    {
        // Validate user input
        $validation = $this->validator->validate($request, [
            'school' => v::notEmpty()->stringType(),
            'major' => v::notEmpty()->stringType(),
            'degree' => v::notEmpty()->stringType(),
        ]);

        $user = User::findOrFail($_SESSION['user']);

        if ($validation->failed()) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et reéssayez');
            return $response->withRedirect($this->router->pathFor('dashboard.studies.new', ['username' => $user->username]));
        }

        $started = "{$request->getParam('start_year')}/{$request->getParam('start_month')}/{$request->getParam('start_day')}";
        $graduated = "{$request->getParam('graduation_year')}/{$request->getParam('graduation_month')}/{$request->getParam('graduation_day')}";

        $study = new Study();

        $study->user_id = $_SESSION['user'];
        $study->school = $request->getParam('school');
        $study->major = $request->getParam('major');
        $study->degree = $request->getParam('degree');
        $study->stated_at = $started;
        $study->finished_at = $graduated;

        if (!$study->save()) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et reéssayez');
            return $response->withRedirect($this->router->pathFor('dashboard.studies.new', ['username' => $user->username]));
        }

        $this->flash->addMessage('success', 'Votre étude à été ajouter');
        return $response->withRedirect($this->router->pathFor('dashboard.studies', ['username' => $user->username]));
    }
}
