<?php

namespace Intelis\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Intelis\Models\User;

class UserController extends Controller
{
    public function edit(Request $request, Response $response)
    {
        $user = User::findOrFail($_SESSION['user']);

        if (!$user) {
            $this->flash->addMessage('info', 'Vous devez être connecté pour faire cela');
            return $response->withRedirect($this->router->pathFor('home'));
        }

        return $this->view->render($response, 'dashboard/users/edit.twig', [
            'user' => $user,
        ]);
    }

    public function updateInfo(Request $request, Response $response)
    {
        // Validate user inputs

        $user = User::findOrFail($_SESSION['user']);

        $user->first_name = $request->getParam('first_name');
        $user->last_name = $request->getParam('last_name');
        $user->phone_number = $request->getParam('phone_number');
        $user->address = $request->getParam('address');
        $user->nationality = $request->getParam('nationality');
        $user->gender = $request->getParam('gender');

        if (!$user->save()) {
            return $response->withRedirect($this->router->pathFor('user.edit', ['username' => $user->username]));
        }

        return $response->withRedirect($this->router->pathFor('dashboard', ['username' => $user->username]));
    }

    public function changePassword(Request $request, Response $response)
    {}
}
