<?php

namespace Intelis\Controllers;

use Slim\Router;
use Slim\Views\Twig as View;
use Slim\Flash\Messages as Flash;
use Awurth\SlimValidation\Validator;
// use Intelis\Validation\Validator;

class Controller
{
    protected $router;

    protected $view;

    protected $flash;

    protected $validator;

    public function __construct(Router $router, View $view, Flash $flash, Validator $validator)
    {
        $this->router = $router;
        $this->view = $view;
        $this->flash = $flash;
        $this->validator = $validator;
    }
}
