<?php

namespace Intelis\Controllers\Admin;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Intelis\Controllers\Controller;
use Intelis\Models\User;

class ProsController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $experts = User::where('is_admin', '=', '0')
            ->orderBy('created_at', 'desc')
            ->get();

        return $this->view->render($response, 'admin/experts/index.twig', [
            'experts' => $experts,
        ]);
    }
}
