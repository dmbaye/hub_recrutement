<?php

namespace Intelis\Controllers\Admin;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Intelis\Controllers\Controller;
use Intelis\Models\User;
use Intelis\Models\Experience;
use Intelis\Models\Study;

class AdminController extends Controller
{
    public function search(Request $request, Response $response)
    {
        return $this->view->render($response, 'admin/search.twig');
    }

    public function result(Request $request, Response $response)
    {
        $results = [];

        return $this->view->render($response, 'admin/results.twig', [
            'results' => $results,
        ]);
    }
}
