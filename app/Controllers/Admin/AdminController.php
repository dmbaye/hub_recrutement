<?php

namespace Intelis\Controllers\Admin;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Intelis\Controllers\Controller;
use Intelis\Models\User;
use Intelis\Auth\Auth;

class AdminController extends Controller
{
    public function index(Request $request, Response $response)
    {
        return $this->view->render($response, 'admin/index.twig');
    }

    public function showLogin(Request $request, Response $response)
    {
        return $this->view->render($response, 'admin/auth/login.twig');
    }

    public function login(Request $request, Response $response)
    {
        Auth::logout();

        // Validate user inputs
        $login = User::whereRaw('email = ? AND is_admin = ?', [$request->getParam('email'), 1])
            ->first();

        if (!$login) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et reéssayez');
            return $response->withRedirect($this->router->pathFor('admin.login'));
        } else if (!password_verify($request->getParam('password'), $login->password)) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et reéssayez');
            return $response->withRedirect($this->router->pathFor('admin.login'));
        }

        $_SESSION['user'] = $login->id;
        $this->flash->addMessage('success', 'Vous vous êtes authentifié(e) avec success');
        return $response->withRedirect($this->router->pathFor('admin'));
    }
}
