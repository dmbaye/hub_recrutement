<?php

namespace Intelis\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as V;
use Intelis\Models\User;
use Intelis\Auth\Auth;

class AuthController extends Controller
{
    public function profile(Request $request, Response $response, string $username)
    {
        $user = User::where('username', $username)->first();

        return $this->view->render($response, 'auth/profile.twig', [
            'user' => $user,
        ]);
    }

    public function showRegister(Request $request, Response $response)
    {
        return $this->view->render($response, 'auth/register.twig');
    }

    // Create a new account
    public function register(Request $request, Response $response)
    {
        // Validate user input
        $validation = $this->validator->validate($request, [
            'first_name' => V::notEmpty()->noWhitespace()->stringType()->length(3, 20, true),
            'last_name' => V::notEmpty()->noWhitespace()->stringType()->length(3, 20, true),
            'username' => V::notEmpty()->noWhitespace()->alnum('_-')->length(3, 20, true),
            'email' => V::notEmpty()->noWhitespace()->stringType()->email()->max(255),
            'password' => V::notEmpty()->noWhitespace()->stringType()->length(6, 20, true),
            'confirm_password' => V::notEmpty()->noWhitespace()->stringType()->length(6, 20, true)->equals($request->getParam('password')),
        ], null, [
            'notEmpty' => 'Reseignez le champ s\'il vous plaît',
            'noWhitespace' => 'Les espaces ne sont pas permis',
            'stringType' => 'Entrez du texte s\'il vous plaît',
            'length' => 'Veuillez saisir au moins 3 caractères',
            'email' => 'Entrez un email valide s\il vous plaît',
            'equals' => 'Les mots de passes sont différents'
        ]);

        if (!$validation->isValid()) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et reéssayez');
            return $response->withRedirect($this->router->pathFor('auth.register'));
        }

        $user = new User();

        $user->first_name = $request->getParam('first_name');
        $user->last_name = $request->getParam('last_name');
        $user->username = $request->getParam('username');
        $user->email = $request->getParam('email');
        $user->password = password_hash($request->getParam('password'), PASSWORD_DEFAULT);

        if (!$user->save()) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et reéssayez');
            return $response->withRedirect($this->router->pathFor('auth.register'));
        }

        $this->flash->addMessage('success', 'Votre compte à été créér avec success');
        return $response->withRedirect($this->router->pathFor('auth.login'));
    }

    public function showLogin(Request $request, Response $response)
    {
        return $this->view->render($response, 'auth/login.twig');
    }

    // Authenticate user
    public function login(Request $request, Response $response)
    {
        // Validate user input
        $validation = $this->validator->validate($request, [
            'email' => V::notEmpty()->noWhitespace()->stringType()->email()->max(255),
            'password' => V::notEmpty()->noWhitespace()->stringType()->length(6, 20, true),
        ], null, [
            'notEmpty' => 'Reseignez le champ s\'il vous plaît',
            'noWhitespace' => 'Les espaces ne sont pas permis',
            'stringType' => 'Entrez du texte s\'il vous plaît',
            'length' => 'Veuillez saisir au moins 3 caractères',
            'email' => 'Entrez un email valide s\il vous plaît',
        ]);

        if ($validation->isValid()) {
            $this->flash->addMessage('error', 'Corrigez les erreurs et reéssayez');
            return $response->withRedirect($this->router->pathFor('auth.login'));
        }

        if (!Auth::attempt($request)) {
            $this->flash->addMessage('error', 'Email/Pseudo ou mot de passe invalide');
            return $response->withRedirect($this->router->pathFor('auth.login'));
        }

        $user = User::findOrFail($_SESSION['user']);

        $this->flash->addMessage('success', 'Vous vous êtes authentifié(e) avec success');
        return $response->withRedirect($this->router->pathFor('dashboard', [
            'username' => $user->username,
        ]));
    }

    // Destroy user session
    public function logout(Request $request, Response $response)
    {
        // Log the user out and redirect to home page
        Auth::logout();
        return $response->withRedirect($this->router->pathFor('home'));
    }
}
