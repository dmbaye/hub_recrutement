<?php

namespace Intelis\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Intelis\Models\User;

class DashboardController extends Controller
{
    public function index(Request $request, Response $response, string $username)
    {
        $user = User::where('username', $username)->with('experiences', 'studies')->first();

        if (!$user) {
            return $response->withRedirect($this->router->pathFor('/'));
        }

        return $this->view->render($response, 'dashboard/index.twig', [
            'user' => $user,
        ]);
    }

    public function edit()
    {

    }

    public function update()
    {

    }
}
