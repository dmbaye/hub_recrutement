<?php

namespace Intelis\Middlewares;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Csrf\Guard as Csrf;

class CsrfViewMiddleware extends Middleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $this->view->getEnvironment()->addGlobal('csrf_token', [
            'field' => '
                <input type="hidden" name="' . Csrf::getTokenNameKey() . '" value="' . Csrf::getTokenName() . '">
                <input type="hidden" name="' . Csrf::getTokenValueKey() . '" value="' . Csrf::getTokenValue() . '">
            '
        ]);

        $response = $next($request, $response);
        return $response;
    }
}
