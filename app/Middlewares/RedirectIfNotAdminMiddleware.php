<?php

namespace Intelis\Middlewares;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Intelis\Models\User;

class RedirectIfAdminMiddleware extends Middleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (isset($_SESSION['user'])) {
            $user = User::findOrFail($_SESSION['user']);

            if ($user->is_admin === 0) {
                return $response->withRedirect($this->router->pathFor('admin'));
            }
        }

        $response = $next($request, $response);
        return $response;
    }
}
