<?php

namespace Intelis\Middlewares;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Intelis\Models\User;

class AdminAuthMiddleware extends Middleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (!isset($_SESSION['user'])) {
            return $response->withRedirect($this->router->pathFor('admin.login'));
        }

        $response = $next($request, $response);
        return $response;
    }
}
