<?php

namespace Intelis\Models;

class Study extends Model
{
    protected $fillable = [
        'user_id',
        'school',
        'major',
        'degree',
        'started_at',
        'finished_at',
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(\Intelis\Models\User::class);
    }
}
