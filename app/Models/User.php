<?php

namespace Intelis\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'username',
        'phone_number',
        'password',
        'birth_date',
        'gender',
        'address',
        'nationality',
        'image',
        'is_admin',
    ];

    protected $hidden = [
        'password'
    ];

    protected $dates = ['deleted_at'];

    public function getFullName()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function studies()
    {
        return $this->hasMany(\Intelis\Models\Study::class);
    }

    public function experiences()
    {
        return $this->hasMany(\Intelis\Models\Experience::class);
    }
}
