<?php

namespace Intelis\Models;

class Experience extends Model
{
    protected $fillable = [
        'user_id',
        'position',
        'company',
        'tasks',
        'started_at',
        'finished_at',
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(\Intelis\Models\User::class);
    }
}
