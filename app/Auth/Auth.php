<?php

namespace Intelis\Auth;

use Psr\Http\Message\ServerRequestInterface as Request;
use Intelis\Models\User;

class Auth
{
    public function attempt(Request $request)
    {
        $login = User::whereRaw('(email = ? OR username = ? ) AND is_admin = ?', [$request->getParam('email'), $request->getParam('email'), 0])
            ->first();

        if (!$login) {
            return false;
        } else if (!password_verify($request->getParam('password'), $login->password)) {
            return false;
        }

        $_SESSION['user'] = $login->id;
        return true;
    }

    public function logout()
    {
        unset($_SESSION['user']);
    }

    public function check()
    {
        return isset($_SESSION['user']);
    }

    public function user()
    {
        return User::findOrFail($_SESSION['user']);
    }
}
