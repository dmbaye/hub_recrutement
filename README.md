# Hub Recrutement

## Installation
$ git clone git@bitbucket.org:dmbaye/hub_recrutement.git
$ composer install ou composer update

## Setup de la base de données
Renommer .env-example en .env et remplacer les champs par les informations de votre base de données

## Execution
$ php -S localhost:8000 -t public
ou
Creer un vhost vers le dossier public du projet
